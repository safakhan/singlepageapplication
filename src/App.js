import './App.css';
 import Navbar from './components/Navbar';
 import {BrowserRouter,Route,Routes,Switch,Link} from 'react-router-dom';
  import Reactmain from './components/Reactmain';
 import Docs from './components/Docs';
 import Blog from './components/Blog';
 import Community from './components/Community';
 import Tutorial from './components/Tutorial';
function App() {
  return (
    <div className="App">
      
      <BrowserRouter>
     <Navbar/>
      <switch>
        <Routes>
          <Route path='/' Component={Reactmain}></Route>
          <Route path='/Docs' Component={Docs}></Route>
          <Route path='/Blog' Component={Blog}></Route>
          <Route path='/Community' Component={Community}></Route>
          <Route path='/Tutorial' Component={Tutorial}></Route>
        </Routes>
      </switch>
    
     </BrowserRouter>
    </div>
  );
}

export default App;
