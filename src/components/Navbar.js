 import React from 'react'
 import {BrowserRouter,Route,Routes,Switch,Link} from 'react-router-dom'

 
 function Navbar() {
    const reactStyle={
        fontWeight:'bolder',
        marginRight:'300px',
        marginLeft:'70px',
        fontSize:'35px',
         textDecoration:'none'
    }
    const linkStyle ={
        margin:'20px',
        color:'white',
        fontSize:'20px',
    }
   return (
     <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <Link style={reactStyle} to="/">React</Link>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item active">
      <Link style={linkStyle} to="/Docs">Docs</Link>
      </li>
      <li className="nav-item">
      <Link style={linkStyle} to="/Tutorial">Tutorial</Link>
      </li>
     
      <li className="nav-item">
      <Link style={linkStyle} to="/Blog">Blog</Link>
      </li>
      <li className="nav-item">
      <Link style={linkStyle} to="/Community">Community</Link>
      </li>
    </ul>
    <form className="form-inline my-2 my-lg-0">
      <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
      <button className="btn btn-outline-danger my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
     </div>
   )
 }
 
 export default Navbar