import React from 'react'
import {Link} from 'react-router-dom';

function Docs() {
    const div1 = { 
        width:'70%',
        height:'100%',
        paddingTop:'50px',
        textAlign:'left',
        paddingLeft:'50px',
    }
    const div2 = { 
        width:'30%',
        height:'100%',
        paddingTop:'60px',
        paddingLeft:'40px',
        textAlign:'left',
        backgroundColor:'lightgray' 
    }
    const linkStyle = {
        color:'black',
    }
    const divStyle ={
        width:'90%',
        height:'100px',
        marginTop:'60px',
        backgroundColor:'#FFA07A',
        padding:'20px',
        marginBottom:'30px'
    }
  return (
    <div className='d-flex'>
         <div style={div1}>
            <h1><strong>Getting Started</strong></h1>
            <div style={divStyle}>
                <p><strong>These docs are old and won’t be updated. Go to react.dev for the new React docs.</strong></p>
                        <p>The new Quick Start teaches modern React and includes live examples.</p>
            </div>
            <p>This page is an overview of the React documentation and related resources.</p><br></br>
            <p>React is a JavaScript library for building user interfaces. Learn what React is all about on our homepage or in the tutorial.</p><br></br><br></br>
            <ul>
                <li>Try React</li>
                <li>Leatn React</li>
                <li> Stay ainformed</li>
                <li>Versioned documentation</li>
                <li>Something Missing</li>
            </ul>
         </div>
         <div style={div2}>
          <Link style={linkStyle}><strong>INSTALLATION</strong></Link><br></br><br></br>
          <Link style={linkStyle}><strong>Getting Started</strong></Link><br></br><br></br>
          <ul>
            <li>Add react to a Website</li>
            <li> Create new react App</li>
            <li>CDN Links</li>
            <li>Release channel</li>
          </ul><br></br> 
          <Link  style={linkStyle}><strong> MAIN CONCEPTS</strong></Link><br></br><br></br>
          <Link style={linkStyle}><strong> ADVANCE GUIDES</strong></Link><br></br><br></br>
          <Link style={linkStyle}><strong> API REFERENCE</strong></Link><br></br><br></br>
          <Link style={linkStyle}><strong> HOOKS</strong></Link><br></br><br></br>
          <Link style={linkStyle}><strong> TESTING</strong></Link><br></br><br></br>
          <Link style={linkStyle}><strong> CONTRIBUTING</strong></Link><br></br><br></br>



         </div>
    </div>
  )
}

export default Docs