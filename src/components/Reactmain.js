import React from 'react'
import {Link} from 'react-router-dom';
function Reactmain() {
    const divStyle ={
        border:'2px solid black',
        height:'500px',
        backgroundColor:'black',
        color:'skyblue',
        paddingTop:'150px',
        
    }
    const contentStyle ={
        color:'white',
        fontSize:'30px'
    }
    const buttonStyle ={
        width:'200px',
        height:'40px',
        margin:'20px',
        backgroundColor:'skyblue',
        border:'none',
    }

  return (
    <>
    <div style={divStyle}>
        <h1 style={{fontSize:'50px'}}>React</h1>
        <p style={contentStyle}>A JavaScript library for building user interfaces</p>
        <button style={ buttonStyle}>Get Started</button>
        <Link>Take the Tutorial</Link>
    </div>
    </>
  )
}

export default Reactmain